FROM maven:3.8.1-openjdk-17-slim
VOLUME /tmp
# ADD /target/*.jar k8s-spring-boot.jar
ENTRYPOINT ["java","-jar","k8s-spring-boot.jar"]